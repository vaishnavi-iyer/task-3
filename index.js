function invertText() {
  var str = document.getElementById("inputString").value 
  str = str.split(" ").reverse().join(" ").split("").reverse().join("").toLowerCase();
  
  var rg = /(^\s*\w{1}|\.\s*\w{1})/gi;
  str = str.replace(rg, function(letter){
      return letter.toUpperCase();
  })
  
  document.getElementById("invertString").innerHTML = str;
}